package com.gelivane.betsportup

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class SportActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sport)
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.main_container,
                MenuGameFragment()
            ).commit()
    }

    fun onBackFragment() {
        supportFragmentManager.popBackStack()
    }

    fun startInfoFragment() {
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.main_container,
                InfoFragment()
            ).addToBackStack(null).commit()
    }


    fun startBasketballGame() {
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.main_container,
                BasketBallFragment()
            ).addToBackStack(null).commit()
    }

    fun startFootballGame() {
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.main_container,
                FootBallFragment()
            ).addToBackStack(null).commit()
    }

    fun startHockeyGame() {
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.main_container,
                HockeyFragment()
            ).addToBackStack(null).commit()
    }
}
