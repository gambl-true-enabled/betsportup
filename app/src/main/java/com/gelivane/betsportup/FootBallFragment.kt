package com.gelivane.betsportup

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.view.*
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.football_fragment.*


class FootBallFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.football_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        back_button.setOnClickListener {
            (activity as SportActivity).onBackFragment()
        }

        game_football_container.addView(DrawView(context))
    }

    internal class DrawView(context: Context?) : SurfaceView(context),

        SurfaceHolder.Callback {
        private var drawThread: DrawThread? = null
        private var countGoal = 0
        val footballGall = BitmapFactory.decodeResource(resources, R.drawable.footbal_goal)
        val footballBall = BitmapFactory.decodeResource(resources, R.drawable.football_ball)
        private var xBall = 0f
        private var yBall = 0f
        private var x1Ball = 0f
        private var y1Ball = 0f
        private var x2Ball = 0f
        private var y2Ball = 0f
        private var xRBall = 450f
        private var yRBall = 1450f

        override fun surfaceChanged(
            holder: SurfaceHolder, format: Int, width: Int,
            height: Int
        ) {
        }

        override fun surfaceCreated(holder: SurfaceHolder) {
            drawThread = DrawThread(getHolder())
            drawThread!!.setRunning(true)
            drawThread!!.start()
        }

        override fun surfaceDestroyed(holder: SurfaceHolder) {
            var retry = true
            drawThread!!.setRunning(false)
            while (retry) {
                try {
                    drawThread!!.join()
                    retry = false
                } catch (e: InterruptedException) {
                }
            }
        }

        override fun onTouchEvent(event: MotionEvent?): Boolean {
            if (event!!.action == MotionEvent.ACTION_DOWN) {
                x1Ball = event.x
                y1Ball = event.y
            } else {
                if (event.action == MotionEvent.ACTION_UP) {
                    x2Ball = event.x
                    y2Ball = event.y
                    val dx = x2Ball - x1Ball
                    val dy = y2Ball - y1Ball
                    xBall = dx / 20f
                    yBall = dy / 20f
                } else {

                }
            }
            return true

        }

        internal inner class DrawThread(private val surfaceHolder: SurfaceHolder) :
            Thread() {
            private var running = false
            fun setRunning(running: Boolean) {
                this.running = running
            }


            override fun run() {
                var canvas: Canvas?
                while (running) {
                    canvas = null
                    try {
                        canvas = surfaceHolder.lockCanvas(null)
                        if (canvas == null) continue
                        canvas.drawColor(Color.WHITE)
                        val paint = Paint()
                        paint.color = Color.BLACK
                        paint.textSize = 100f
                        val typeface = ResourcesCompat.getFont(context, R.font.bebas_neue_bold);
                        paint.typeface = typeface
                        canvas.drawText("Your score:", 300f, 150f, paint)
                        canvas.drawText(countGoal.toString(), 730f, 150f, paint)
                        canvas.drawBitmap(footballGall, 350f, 250f, Paint())
                        xRBall += xBall
                        yRBall += yBall
                        if (xRBall > 350f && xRBall < 600f && yRBall > 250f && yRBall < 350f) {
                            countGoal += 1
                            xBall = 0f
                            yBall = 0f
                            xRBall = 450f
                            yRBall = 1450f
                        }
                        if (yRBall < 0f || xRBall < 0f || yRBall > 1800f || xRBall > 1000f) {
                            xBall = 0f
                            yBall = 0f
                            xRBall = 450f
                            yRBall = 1450f
                        }
                        canvas.drawBitmap(footballBall, xRBall, yRBall, Paint())
                    } finally {
                        if (canvas != null) {
                            surfaceHolder.unlockCanvasAndPost(canvas)
                        }
                    }
                }
            }

        }

        init {
            holder.addCallback(this)
        }
    }
}