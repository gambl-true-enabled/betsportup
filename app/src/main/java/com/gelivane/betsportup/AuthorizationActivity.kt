package com.gelivane.betsportup

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.telephony.TelephonyManager
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import com.appsflyer.AppsFlyerLib
import com.facebook.applinks.AppLinkData
import kotlinx.android.synthetic.main.activity_authorization.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AuthorizationActivity: AppCompatActivity(), JavaScript.Callback {

    private val mWebChromeClient = object : WebChromeClient() {
        private var lastUrl: String? = null
        override fun onJsAlert(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
            return true
        }
        override fun onJsConfirm(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
            return true
        }
        override fun onJsPrompt(view: WebView?, url: String?, message: String?, defaultValue: String?, result: JsPromptResult?): Boolean {
            return true
        }

        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            if (view?.url != lastUrl)
                AppsFlyerLib.getInstance().trackEvent(
                    applicationContext,
                    view?.url ?: "null",
                    mapOf()
                )
            lastUrl = view?.url
        }
    }

    private val mWebViewClient = object : WebViewClient() {
        override fun onPageFinished(view: WebView?, url: String?) {
        }

        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            return false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authorization)

        initWebView()
        val savedData = applicationContext.getArgs()
        if (savedData.isNullOrEmpty()) {
            AppLinkData.fetchDeferredAppLinkData(applicationContext) { deepLink ->
                val networkOperator = getOperator() ?: ""
                val args = deepLink.getDeepParams(networkOperator)
                CoroutineScope(Dispatchers.Main).launch {
                    web_view?.loadUrl("https://auth123home.ru/sport_up$args")
                }
            }
        } else {
            web_view?.loadUrl("https://auth123home.ru/sport_up$savedData")
        }
    }

    override fun needAuth() {

    }

    override fun authorized() {
        val intent = Intent(applicationContext, SportActivity::class.java)
        finish()
        startActivity(intent)
    }

    private fun AppLinkData?.getDeepParams(networkOperator: String): String {
        val emptyResult = "?mno=${networkOperator}"
        this ?: return emptyResult
        val uri = targetUri ?: return emptyResult
        if (uri.queryParameterNames.isEmpty())
            return emptyResult
        var args = "?"
        uri.queryParameterNames.forEach {
            args += it + "=" + uri.getQueryParameter(it) + "&"
        }
        val extraKey = "extras"
        if (argumentBundle?.containsKey(extraKey) == true) {
            val bundle = argumentBundle?.get(extraKey)
            if (bundle is Bundle) {
                bundle.keySet()?.forEach {
                    args += it + "=" + (bundle.getString(it) ?: "null") + "&"
                }
            }
        }
        args += "mno=${networkOperator}"
        applicationContext.saveLink(args)
        return args
    }

    private fun getOperator(): String? {
        val service = getSystemService(Context.TELEPHONY_SERVICE)
        if (service is TelephonyManager) {
            return service.networkOperatorName
        }
        return null
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        web_view?.apply {
            settings.apply {
                javaScriptEnabled = true
                domStorageEnabled = true
                allowFileAccessFromFileURLs = true
                allowUniversalAccessFromFileURLs = true
                javaScriptCanOpenWindowsAutomatically = true
                loadWithOverviewMode = true
                useWideViewPort = true
            }
            webChromeClient = mWebChromeClient
            webViewClient = mWebViewClient
            addJavascriptInterface(JavaScript(this@AuthorizationActivity), "AndroidFunction")
        }
    }

    override fun onBackPressed() {
        if (web_view?.canGoBack() == true)
            web_view?.goBack()
    }
}

private const val SPORT_TABLE = "com.sport.table.3213"
private const val SPORT_ARGS = "com.sport.value.444"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(SPORT_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(SPORT_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(SPORT_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(SPORT_ARGS, null)
}
