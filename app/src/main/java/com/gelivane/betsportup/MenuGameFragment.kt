package com.gelivane.betsportup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.menu_fragment.*

class MenuGameFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.menu_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        info_icon.setOnClickListener {
            (activity as SportActivity).startInfoFragment()
        }
        basketball_container.setOnClickListener {
            (activity as SportActivity).startBasketballGame()
        }
        football_container.setOnClickListener {
            (activity as SportActivity).startFootballGame()
        }
        hockey_container.setOnClickListener {
            (activity as SportActivity).startHockeyGame()
        }
    }
}